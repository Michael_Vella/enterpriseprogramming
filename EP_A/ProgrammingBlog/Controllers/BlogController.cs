﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProgrammingBlog.Models;

namespace ProgrammingBlog.Controllers
{
    public class BlogController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Blog
        public ActionResult Index()
        {
            List<BlogModel> blogModels = new List<BlogModel>();
            List<BlogModel> latestsPosts = new List<BlogModel>();

            BlogCategoryModel blogCategoryModel = new BlogCategoryModel();

            //Gets the 5 most recent posts
            if (db.Blogs.Count() > 0 && db.Categories.Count() > 0)
            {
                blogModels = db.Blogs.OrderByDescending(x => x.dateTime).ToList();
                latestsPosts = blogModels.Take(5).ToList();
            }

            //populating empty entities according to their repsective blogs
            if (db.Blogs.Count() > 0 && db.Categories.Count() > 0)
            {
                blogCategoryModel.CSharp = db.Blogs.OrderByDescending(x => x.dateTime).Where(cSharp => cSharp.Category.CategoryTitle == "C#").ToList();
                blogCategoryModel.CSS = db.Blogs.OrderByDescending(x => x.dateTime).Where(css => css.Category.CategoryTitle == "CSS").ToList();
                blogCategoryModel.MVC = db.Blogs.OrderByDescending(x => x.dateTime).Where(mvc => mvc.Category.CategoryTitle == "MVC ASP.NET").ToList();
                blogCategoryModel.Javascript = db.Blogs.OrderByDescending(x => x.dateTime).Where(js => js.Category.CategoryTitle == "Javascript").ToList();
                blogCategoryModel.Blog = latestsPosts;
            }

            
              return View(blogCategoryModel);
        }
        
        
        public ActionResult SpecificCategory(string category) {
            if (category.Equals("C#"))
            {
                return RedirectToAction("CSharpBlogPosts", "Blog");
            }
            else if (category.Equals("Javascript"))
            {
                return RedirectToAction("JavascriptBlogPosts", "Blog");
            }
            else if (category.Equals("MVC ASP.NET"))
            {
                return RedirectToAction("MVCBlogPosts", "Blog");
            }
            else if (category.Equals("CSS"))
            {
                return RedirectToAction("CssBlogPosts", "Blog");
            }

            return RedirectToAction("Index", "Blog");

        }
        // GET: Blog
        public ActionResult CSharpBlogPosts()
        {
            BlogCategoryModel blogCategoryModel = new BlogCategoryModel();
            blogCategoryModel.CSharp = db.Blogs.OrderByDescending(x => x.dateTime).Where(cSharp => cSharp.Category.CategoryTitle == "C#").ToList();
            return View(blogCategoryModel);
        }

        // GET: Blog
        public ActionResult MVCBlogPosts()
        {
            BlogCategoryModel blogCategoryModel = new BlogCategoryModel();
            blogCategoryModel.MVC = db.Blogs.OrderByDescending(x => x.dateTime).Where(mvc => mvc.Category.CategoryTitle == "MVC ASP.NET").ToList();
            return View(blogCategoryModel);
        }

        // GET: Blog
        public ActionResult JavascriptBlogPosts()
        {
            BlogCategoryModel blogCategoryModel = new BlogCategoryModel();
            blogCategoryModel.Javascript = db.Blogs.OrderByDescending(x => x.dateTime).Where(js => js.Category.CategoryTitle == "Javascript").ToList();
            return View(blogCategoryModel);
        }

        // GET: Blog
        public ActionResult CssBlogPosts()
        {
            BlogCategoryModel blogCategoryModel = new BlogCategoryModel();
            blogCategoryModel.CSS = db.Blogs.OrderByDescending(x => x.dateTime).Where(css => css.Category.CategoryTitle == "CSS").ToList();
            return View(blogCategoryModel);
        }

        // GET: Blog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogModel blogModel = db.Blogs.Find(id);
            if (blogModel == null)
            {
                return HttpNotFound();
            }
            return View(blogModel);
        }

        [Authorize]
        // GET: Blog/Create
        public ActionResult Create()
        {
            ViewBag.Categories = new SelectList(db.Categories, "CategoryID", "CategoryTitle");
            return View();
        }

        // POST: Blog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BlogID,BlogHeading,BlogPost,BlogTags"), ] BlogModel blogModel, int CategoryId, HttpPostedFileBase imagePath)
        {

            blogModel.Users = db.Users.Where(x => x.UserName == System.Web.HttpContext.Current.User.Identity.Name).FirstOrDefault();
            blogModel.Category = db.Categories.Where(x => x.CategoryId == CategoryId).FirstOrDefault();
            blogModel.dateTime = DateTime.Now;
            if (ModelState.IsValid)
            {
                if ( imagePath != null)
                {
                    string absolutePath = Server.MapPath("\\BlogImages\\");
                    string relativePath = "\\BlogImages\\";
                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(imagePath.FileName);
                    imagePath.SaveAs(absolutePath + fileName);//saving the image inside folder
                    blogModel.ImagePath = relativePath + fileName; //saving path inside the User instance that will be saved in db

                    db.Blogs.Add(blogModel);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.Blogs.Add(blogModel);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
              
            }

            return View(blogModel);
        }

        // GET: Blog/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogModel blogModel = db.Blogs.Find(id);
            if (blogModel == null)
            {
                return HttpNotFound();
            }
            ViewBag.Categories = new SelectList(db.Categories, "CategoryID", "CategoryTitle");
            return View(blogModel);
        }

        // POST: Blog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "BlogID,BlogHeading,BlogPost,BlogTags")] BlogModel blogModel, int blogId, int CategoryId, HttpPostedFileBase imagePath)
        {
            blogModel.Users = db.Users.Where(x => x.UserName == System.Web.HttpContext.Current.User.Identity.Name).FirstOrDefault();
            blogModel.Category = db.Categories.Where(x => x.CategoryId == CategoryId).FirstOrDefault();
            blogModel.dateTime = DateTime.Now;
            blogModel.BlogID = blogId;
            if (ModelState.IsValid)
            {
                if (imagePath != null)
                {
                    string absolutePath = Server.MapPath("\\BlogImages\\");
                    string relativePath = "\\BlogImages\\";
                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(imagePath.FileName);
                    imagePath.SaveAs(absolutePath + fileName);//saving the image inside folder
                    blogModel.ImagePath = relativePath + fileName; //saving path inside the User instance that will be saved in db
                    db.Entry(blogModel).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else { 
                db.Entry(blogModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
                }
            }
            return View(blogModel);
        }

        // GET: Blog/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogModel blogModel = db.Blogs.Find(id);
            if (blogModel == null)
            {
                return HttpNotFound();
            }
            return View(blogModel);
        }

        // POST: Blog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            BlogModel blogModel = db.Blogs.Find(id);
            db.Blogs.Remove(blogModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
