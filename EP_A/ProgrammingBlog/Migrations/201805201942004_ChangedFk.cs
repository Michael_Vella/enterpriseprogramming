namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedFk : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels");
            DropIndex("dbo.BlogModels", new[] { "Category_CategoryId" });
            RenameColumn(table: "dbo.BlogModels", name: "Category_CategoryId", newName: "CategoryId");
            AlterColumn("dbo.BlogModels", "CategoryId", c => c.Int(nullable: true));
            CreateIndex("dbo.BlogModels", "CategoryId");
            AddForeignKey("dbo.BlogModels", "CategoryId", "dbo.CategoryModels", "CategoryId", cascadeDelete: true);
            DropColumn("dbo.BlogModels", "Category_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogModels", "Category_Id", c => c.Int(nullable: false));
            DropForeignKey("dbo.BlogModels", "CategoryId", "dbo.CategoryModels");
            DropIndex("dbo.BlogModels", new[] { "CategoryId" });
            AlterColumn("dbo.BlogModels", "CategoryId", c => c.Int());
            RenameColumn(table: "dbo.BlogModels", name: "CategoryId", newName: "Category_CategoryId");
            CreateIndex("dbo.BlogModels", "Category_CategoryId");
            AddForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels", "CategoryId");
        }
    }
}
