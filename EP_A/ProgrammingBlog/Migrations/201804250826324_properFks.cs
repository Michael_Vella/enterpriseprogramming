namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class properFks : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Categories", "Blog_BlogID", "dbo.Blogs");
            DropIndex("dbo.Categories", new[] { "Blog_BlogID" });
            AddColumn("dbo.Blogs", "Category_CategoryId", c => c.Int());
            CreateIndex("dbo.Blogs", "Category_CategoryId");
            AddForeignKey("dbo.Blogs", "Category_CategoryId", "dbo.Categories", "CategoryId");
            DropColumn("dbo.Categories", "Blog_BlogID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Categories", "Blog_BlogID", c => c.Int());
            DropForeignKey("dbo.Blogs", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Blogs", new[] { "Category_CategoryId" });
            DropColumn("dbo.Blogs", "Category_CategoryId");
            CreateIndex("dbo.Categories", "Blog_BlogID");
            AddForeignKey("dbo.Categories", "Blog_BlogID", "dbo.Blogs", "BlogID");
        }
    }
}
