namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.Int(nullable: false));
        }
    }
}
