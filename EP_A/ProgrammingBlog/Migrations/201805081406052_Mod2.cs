namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mod2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blogs", "BlogTitle", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Blogs", "BlogContent", c => c.String(nullable: false));
            AlterColumn("dbo.Blogs", "BlogTags", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Blogs", "BlogTags", c => c.String());
            AlterColumn("dbo.Blogs", "BlogContent", c => c.String());
            AlterColumn("dbo.Blogs", "BlogTitle", c => c.String());
        }
    }
}
