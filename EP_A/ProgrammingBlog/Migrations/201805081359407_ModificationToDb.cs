namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModificationToDb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "BlogContent", c => c.String());
            AlterColumn("dbo.Categories", "CategoryTitle", c => c.String(nullable: false));
            DropColumn("dbo.Blogs", "BlogContnet");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Blogs", "BlogContnet", c => c.String());
            AlterColumn("dbo.Categories", "CategoryTitle", c => c.String());
            DropColumn("dbo.Blogs", "BlogContent");
        }
    }
}
