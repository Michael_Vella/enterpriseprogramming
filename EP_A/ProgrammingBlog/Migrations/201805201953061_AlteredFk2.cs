namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "User_Id", c => c.Int(nullable: false));
            DropColumn("dbo.BlogModels", "UserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogModels", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.BlogModels", "User_Id");
        }
    }
}
