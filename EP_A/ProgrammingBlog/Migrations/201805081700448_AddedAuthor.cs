namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAuthor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "Users_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.BlogModels", "Users_Id");
            AddForeignKey("dbo.BlogModels", "Users_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlogModels", "Users_Id", "dbo.AspNetUsers");
            DropIndex("dbo.BlogModels", new[] { "Users_Id" });
            DropColumn("dbo.BlogModels", "Users_Id");
        }
    }
}
