namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlogModels", "Users_Id", "dbo.AspNetUsers");
            DropIndex("dbo.BlogModels", new[] { "Users_Id" });
            AddColumn("dbo.BlogModels", "Users_Id1", c => c.String(maxLength: 128));
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.Int(nullable: true));
            CreateIndex("dbo.BlogModels", "Users_Id1");
            AddForeignKey("dbo.BlogModels", "Users_Id1", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.BlogModels", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlogModels", "User_Id", c => c.Int(nullable: false));
            DropForeignKey("dbo.BlogModels", "Users_Id1", "dbo.AspNetUsers");
            DropIndex("dbo.BlogModels", new[] { "Users_Id1" });
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.BlogModels", "Users_Id1");
            CreateIndex("dbo.BlogModels", "Users_Id");
            AddForeignKey("dbo.BlogModels", "Users_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
