namespace ProgrammingBlog.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ProgrammingBlog.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProgrammingBlog.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProgrammingBlog.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            context.Users.AddOrUpdate( new ApplicationUser { UserName = "test@email.com", Email = "test@email.com", SecurityStamp = Guid.NewGuid().ToString() ,PasswordHash = userManager.PasswordHasher.HashPassword("test")});

            context.Categories.AddOrUpdate(new Models.CategoryModel { CategoryTitle = "C#" },
                                           new Models.CategoryModel { CategoryTitle = "MVC ASP.NET" },
                                           new Models.CategoryModel { CategoryTitle = "Javascript" },
                                           new Models.CategoryModel { CategoryTitle = "CSS" }
                                           );

           context.SaveChanges();

            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test C# POST 1", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now,  Category = context.Categories.Where(x => x.CategoryTitle =="C#").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test C# POST 2", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "C#").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test C# POST 3", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "C#").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test C# POST 4", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "C#").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test Javascript POST 1", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "Javascript").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test Javascript POST 2", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "Javascript").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test Javascript POST 3", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "Javascript").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test Javascript POST 4", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "Javascript").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
           
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test CSS POST 1", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "CSS").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test CSS POST 2", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "CSS").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test CSS POST 3", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "CSS").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test CSS POST 4", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "CSS").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
           
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test MVC POST 1", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "MVC ASP.NET").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test MVC POST 2", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "MVC ASP.NET").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test MVC POST 3", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "MVC ASP.NET").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test MVC POST 4", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "MVC ASP.NET").FirstOrDefault(), Users = context.Users.FirstOrDefault() });

            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test C# POST 5", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "C#").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test Javascript POST 5", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "Javascript").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test CSS POST 5", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "CSS").FirstOrDefault(), Users = context.Users.FirstOrDefault() });
            context.Blogs.AddOrUpdate(new Models.BlogModel { BlogHeading = "Test MVC POST 5", BlogPost = "HELLLLLLLLOOOOO THERE", BlogTags = "Some, stuff , here", ImagePath = "\\BlogImages\\stockImage.jpg", dateTime = DateTime.Now, Category = context.Categories.Where(x => x.CategoryTitle == "MVC ASP.NET").FirstOrDefault(), Users = context.Users.FirstOrDefault() });

            context.SaveChanges();
        }
    }
}