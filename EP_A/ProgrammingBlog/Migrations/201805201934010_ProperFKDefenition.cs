namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProperFKDefenition : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "Category_Id", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogModels", "Category_Id");
        }
    }
}
