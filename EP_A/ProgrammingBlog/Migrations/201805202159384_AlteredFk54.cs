namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk54 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels");
            DropIndex("dbo.BlogModels", new[] { "Category_CategoryId" });
            AlterColumn("dbo.BlogModels", "Category_CategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.BlogModels", "Category_CategoryId");
            AddForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels", "CategoryId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels");
            DropIndex("dbo.BlogModels", new[] { "Category_CategoryId" });
            AlterColumn("dbo.BlogModels", "Category_CategoryId", c => c.Int());
            CreateIndex("dbo.BlogModels", "Category_CategoryId");
            AddForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels", "CategoryId");
        }
    }
}
