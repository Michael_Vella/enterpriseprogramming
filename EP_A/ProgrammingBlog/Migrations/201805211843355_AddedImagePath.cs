namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedImagePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "ImagePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogModels", "ImagePath");
        }
    }
}
