// <auto-generated />
namespace ProgrammingBlog.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlteredFk6 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlteredFk6));
        
        string IMigrationMetadata.Id
        {
            get { return "201805202208002_AlteredFk6"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
