namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mod3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Blogs", "Category_CategoryId", "dbo.Categories");
            DropIndex("dbo.Blogs", new[] { "Category_CategoryId" });
            CreateTable(
                "dbo.BlogModels",
                c => new
                    {
                        BlogID = c.Int(nullable: false, identity: true),
                        BlogHeading = c.String(nullable: false, maxLength: 50),
                        BlogPost = c.String(nullable: false),
                        BlogTags = c.String(nullable: false),
                        Category_CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.BlogID)
                .ForeignKey("dbo.CategoryModels", t => t.Category_CategoryId)
                .Index(t => t.Category_CategoryId);
            
            CreateTable(
                "dbo.CategoryModels",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryTitle = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            DropTable("dbo.Blogs");
            DropTable("dbo.Categories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryTitle = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        BlogID = c.Int(nullable: false, identity: true),
                        BlogTitle = c.String(nullable: false, maxLength: 50),
                        BlogContent = c.String(nullable: false),
                        BlogTags = c.String(nullable: false),
                        Category_CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.BlogID);
            
            DropForeignKey("dbo.BlogModels", "Category_CategoryId", "dbo.CategoryModels");
            DropIndex("dbo.BlogModels", new[] { "Category_CategoryId" });
            DropTable("dbo.CategoryModels");
            DropTable("dbo.BlogModels");
            CreateIndex("dbo.Blogs", "Category_CategoryId");
            AddForeignKey("dbo.Blogs", "Category_CategoryId", "dbo.Categories", "CategoryId");
        }
    }
}
