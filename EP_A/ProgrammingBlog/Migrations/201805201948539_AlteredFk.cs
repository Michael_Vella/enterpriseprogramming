namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "UserId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogModels", "UserId");
        }
    }
}
