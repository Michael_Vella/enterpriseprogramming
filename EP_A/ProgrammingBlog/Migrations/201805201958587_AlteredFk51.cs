namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlteredFk51 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.BlogModels", new[] { "Users_Id1" });
            DropColumn("dbo.BlogModels", "Users_Id");
            RenameColumn(table: "dbo.BlogModels", name: "Users_Id1", newName: "Users_Id");
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.BlogModels", "Users_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BlogModels", new[] { "Users_Id" });
            AlterColumn("dbo.BlogModels", "Users_Id", c => c.String());
            RenameColumn(table: "dbo.BlogModels", name: "Users_Id", newName: "Users_Id1");
            AddColumn("dbo.BlogModels", "Users_Id", c => c.String());
            CreateIndex("dbo.BlogModels", "Users_Id1");
        }
    }
}
