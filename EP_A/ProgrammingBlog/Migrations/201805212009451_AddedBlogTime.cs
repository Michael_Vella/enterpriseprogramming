namespace ProgrammingBlog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBlogTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BlogModels", "dateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BlogModels", "dateTime");
        }
    }
}
