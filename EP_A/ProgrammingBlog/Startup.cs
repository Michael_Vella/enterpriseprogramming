﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProgrammingBlog.Startup))]
namespace ProgrammingBlog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
