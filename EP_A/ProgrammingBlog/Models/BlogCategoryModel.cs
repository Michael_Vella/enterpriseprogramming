﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgrammingBlog.Models
{
    public class BlogCategoryModel
    {
        public List<BlogModel> CSharp { get; set; }
        public List<BlogModel> Javascript { get; set; }
        public List<BlogModel> CSS { get; set; }
        public List<BlogModel> MVC { get; set; }
        public List<BlogModel> Blog { get; set; }
        public List<CategoryModel> Category { get; set; }
    }
}