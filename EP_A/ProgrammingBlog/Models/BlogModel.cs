﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProgrammingBlog.Models
{
    public class BlogModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BlogID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage ="Total length of Title should be 50 ")]
        [Display(Name = "Title")]
        public string BlogHeading { get; set; }
        [Required]
        [Display(Name = "Post")]
        public string BlogPost { get; set; }
        [Required]
        [Display(Name = "Tags")]
        public string BlogTags { get; set; }
        public DateTime dateTime { get; set; }
        [Display (Name = "Upload Image")]
        public string ImagePath { get; set; }
        public virtual CategoryModel Category { get; set; }
        public virtual ApplicationUser Users { get; set; }
    }
}